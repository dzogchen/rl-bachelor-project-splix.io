#ifndef TORCHMLP_H
#define TORCHMLP_H

#include <torch/torch.h>

#include <vector>
#include <memory>
#include <filesystem>

class TorchMLP: public torch::nn::Module
{
    double d_learning_rate = 0.001;
    torch::nn::Linear in{nullptr};
    torch::nn::Linear h{nullptr};
    torch::nn::Linear out{nullptr};
    std::shared_ptr<torch::optim::SGD> optimizer;
    std::shared_ptr<torch::Device> device;
    std::vector<size_t> d_nodes_per_layer;

  public:
    TorchMLP(double learning_rate = 0.001);
    void init(std::vector<size_t> nodes_per_layer);
    void init(std::vector<size_t> nodes_per_layer, std::filesystem::path file);

    void train(std::pair<torch::Tensor&, torch::Tensor&> example);
//    void train(std::pair<torch::Tensor const&, double const> example);

    // the columns of the 'samples' parameter are vectors in the mini-batch
//    void train(Eigen::MatrixXd samples, Eigen::MatrixXd labels);

    torch::Tensor forward(torch::Tensor input);
//    void print();
    void save(std::filesystem::path file) const;

  private:
//    void setup_vectors(std::vector<size_t> const &nodes_per_layer);
//    void initialize_weights_and_biases();
};

#endif // TORCHMLP_H
