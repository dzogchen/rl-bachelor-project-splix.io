#include "torchmlp.h"

#include <torch/serialize.h>
#include <fstream>

TorchMLP::TorchMLP(double learning_rate)
    : d_learning_rate(learning_rate)
{
}

void TorchMLP::init(std::vector<size_t> nodes_per_layer, std::filesystem::path file) {
    init(nodes_per_layer);
    torch::serialize::InputArchive in;
    in.load_from(file);
    torch::nn::Module::load(in);
}

void TorchMLP::init(std::vector<size_t> nodes_per_layer) {
    assert(nodes_per_layer.size() == 3);
    d_nodes_per_layer = nodes_per_layer;

    h = register_module("h",torch::nn::Linear(nodes_per_layer[0],nodes_per_layer[1]));
    out = register_module("out",torch::nn::Linear(nodes_per_layer[1],nodes_per_layer[2]));
    optimizer = std::make_shared<torch::optim::SGD>(
        parameters(), torch::optim::SGDOptions(d_learning_rate));
    torch::DeviceType device_type;
    if (torch::cuda::is_available()) {
        std::cout << "CUDA available! Training on GPU." << std::endl;
        device_type = torch::kCUDA;
    } else {
        std::cout << "Training on CPU." << std::endl;
        device_type = torch::kCPU;
    }
    device = std::make_shared<torch::Device>(device_type);
    to(*device);
}

torch::Tensor TorchMLP::forward(torch::Tensor x) {
    x = x.to(*device);
    x = torch::sigmoid(h->forward(x));
    x = out->forward(x);
    return x;
}

void TorchMLP::save(std::filesystem::path file) const {
    torch::serialize::OutputArchive out;
    torch::nn::Module::save(out);
    out.save_to(file);
}

void TorchMLP::train(std::pair<torch::Tensor&, torch::Tensor&> example) {
    optimizer->zero_grad();

    auto [input, expected_output] = example;
    expected_output = expected_output.to(*device);
    if (expected_output.sizes().size() > 1 && expected_output.sizes()[1] == 1) {
        expected_output = expected_output.view(expected_output.size(0)); // online learning
    }
    auto const output = forward(input);
    auto loss = torch::mse_loss(output, expected_output.detach());
    loss.backward();
    optimizer->step();
}
