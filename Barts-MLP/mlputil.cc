#include "mlputil.h"

double sigmoid_prime(double x) {
    double y = sigmoid(x);
    return y * (1 - y);
}

double sigmoid(double x)
{
    return 1 / (1 + exp(-x));
}

Eigen::VectorXd num_to_vec(int num) {
    assert(num >= 0 && num <= 9);
    Eigen::VectorXd vec = Eigen::VectorXd::Zero(10);
    vec[num] = 1.0;
    return vec;
}
