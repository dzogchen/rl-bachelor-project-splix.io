#ifndef GRID_H
#define GRID_H

#include "player.h"
#include "location.h"

#include <Eigen/Dense>
#include <torch/torch.h>

#include <vector>
#include <cassert>
#include <optional>
#include <set>
#include <queue>
#include <memory>
#include <iostream>

enum class VISIONGRIDTYPE {
    WALL,
    TAIL,
    ENEMYTAIL,
    ENEMYHEAD,
    OWNEDAREA,
    ENEMYAREA
};

template <long Height, long Width>
class Grid {
    Eigen::Matrix<PlayerID, Height, Width> d_tailsAndHeads = Eigen::Matrix<PlayerID, Height, Width>::Zero(); // stores tails and heads
    Eigen::Matrix<PlayerID, Height, Width> d_owners  = Eigen::Matrix<PlayerID, Height, Width>::Zero(); // stores what player ownes which square
    static size_t constexpr HEADSHIFT = 70;

public:
    Grid()
    {
    }
    inline PlayerID occupiedIndex(int row, int col) const {
        return d_tailsAndHeads(row, col);
    }

    inline PlayerID ownedIndex(Location loc) const {
        return d_owners(loc.row(), loc.col());
    }

    inline PlayerID ownedIndex(int row, int col) const {
        return d_owners(row, col);
    }

    inline auto rows() const {
        return d_owners.rows();
    }

    inline auto cols() const {
        return d_owners.cols();
    }

    inline void head(Location location, PlayerID playerId) {
        d_tailsAndHeads(location.row(), location.col()) = playerId + HEADSHIFT;
    }

    inline void tail(Location location, PlayerID playerId) {
        d_tailsAndHeads(location.row(), location.col()) = playerId;
    }

    enum {
        MAYBEFILL,
        OWNED,
        DONOTFILL,
        FILL
    };

    static void floodFill(Eigen::Matrix<PlayerID, Height + 2, Width + 2> &mat, int row, int col) {
        mat(row, col) = DONOTFILL;
        if (col - 1 > 0 && mat(row, col - 1) == MAYBEFILL)
            floodFill(mat, row, col - 1);
        if (col + 1 < mat.cols() && mat(row, col + 1) == MAYBEFILL)
            floodFill(mat, row, col + 1);
        if (row + 1 < mat.rows() && mat(row + 1, col) == MAYBEFILL)
            floodFill(mat, row + 1, col);
        if (row - 1 > 0 && mat(row - 1, col) == MAYBEFILL)
            floodFill(mat, row - 1, col);
    }

    void endTail(std::shared_ptr<Player> player,
                 std::unordered_map<PlayerID, std::shared_ptr<Player>> &playerIdToPlayer) {
        for (auto const& tailLoc : player->tail()) {
            resetOccupation(tailLoc);
            if (auto currOwner = d_owners(tailLoc.row(), tailLoc.col());
                currOwner != 0 && currOwner != player->id()) {
                playerIdToPlayer[currOwner]->givePoints(-1, true);
            }
            player->givePoints(1); // tail, so not owned by player yet
            owner(tailLoc, player->id());
        }

        Eigen::Matrix<PlayerID, Height + 2, Width + 2> fillMatrix;
        fillMatrix = Eigen::Matrix<PlayerID, Height + 2, Width + 2>::Zero(); // initialize with MAYBEFILL == 0

        for (int row = 0; row != d_owners.rows(); ++row) {
            for (int col = 0; col != d_owners.cols(); ++col) {
                if (d_owners(row, col) == player->id()) {
                    fillMatrix(row + 1, col + 1) = OWNED;
                }
            }
        }


        floodFill(fillMatrix, 0, 0);
        for (int row = 0; row != d_owners.rows(); ++row) {
            for (int col = 0; col != d_owners.cols(); ++col) {
                if (fillMatrix(row + 1, col + 1) == MAYBEFILL && d_owners(row, col) != player->id()) {
                    player->givePoints(1);
                    if (d_owners(row, col) != 0)
                        playerIdToPlayer[d_owners(row, col)]->givePoints(-1, true);
                    d_owners(row, col) = player->id();
                }
            }
        }

    }

    inline void resetOccupation(Location location) {
        d_tailsAndHeads(location.row(), location.col()) = 0;
    }

    inline void resetOwner(Location location) {
        d_owners(location.row(), location.col()) = 0;
    }

    inline void owner(Location location, PlayerID playerId) {
        d_owners(location.row(), location.col()) = playerId;
    }

    std::optional<std::pair<Player::Part, PlayerID>> occupationStatus(Location location) const {
        auto index = d_tailsAndHeads(location.row(), location.col());
        if (index > HEADSHIFT)
            return {{Player::HEAD, index - HEADSHIFT}};
        else if (index != 0)
            return {{Player::TAIL, index}};
        return {}; // index == 0, square is not occupied
    }

    bool isBorder(Location loc) const {
        if (loc.row() == 0 || loc.col() == 0)
            return true;
        if (loc.row() == rows() - 1 || loc.col() == cols() - 1)
            return true;
        return false;
    }

    bool inBounds(Location loc) const {
        if (loc.row() < 0 || loc.col() < 0)
            return false;
        if (loc.row() > rows() - 1 || loc.col() > cols() - 1)
            return false;
        return true;
    }

    template <VISIONGRIDTYPE type>
    void readVisionGrid(torch::Tensor &vGrid, PlayerID idx,
                        std::shared_ptr<Player> player, int size) const {
        Location loc = player->location();
        assert(size % 2 != 0); // size must be uneven

        int const halfsize = size / 2;
        int rowMin = loc.row() - halfsize;
        int rowMax = loc.row() + halfsize + 1;
        int colMin = loc.col() - halfsize;
        int colMax = loc.col() + halfsize + 1;

        switch (player->direction()) {
        case (Direction::N):
            for (int row = rowMin; row < rowMax; ++row) {
                for (int col = colMin; col < colMax; ++col) {
                    vGrid[idx++] = visonGridValue<type>({row, col}, player->id());
                }
            }
            return;
        case (Direction::E):
            for (int col = colMax; --col >= colMin; ) {
                for (int row = rowMin; row < rowMax; ++row) {
                    vGrid[idx++] = visonGridValue<type>({row, col}, player->id());
                }
            }
            return;
        case (Direction::S):
            for (int row = rowMax; --row >= rowMin; ) {
                for (int col = colMax; --col >= colMin; ) {
                    vGrid[idx++] = visonGridValue<type>({row, col}, player->id());
                }
            }
            return;
        case (Direction::W):
            for (int col = colMin; col < colMax; ++col) {
                for (int row = rowMax; --row >= rowMin; ) {
                    vGrid[idx++] = visonGridValue<type>({row, col}, player->id());

                }
            }
            return;
        }
    }

private:
    std::list<Location> neighbors(Location loc) {
        std::list<Location> neighbors;
        neighbors.push_back(loc.move(Direction::N));
        neighbors.push_back(loc.move(Direction::E));
        neighbors.push_back(loc.move(Direction::S));
        neighbors.push_back(loc.move(Direction::W));

        std::remove_if(neighbors.begin(), neighbors.end(), [this, loc](auto newLoc) {
           return not inBounds(newLoc);
        });

        return neighbors;
    }

    template <VISIONGRIDTYPE type>
    double visonGridValue(Location loc, PlayerID forPlayer) const {
        if (not inBounds(loc))
            return 0;
        if constexpr (type == VISIONGRIDTYPE::WALL)
            return isBorder(loc) ? 1 : 0;
        if constexpr (type == VISIONGRIDTYPE::OWNEDAREA)
            return ownedIndex(loc) == forPlayer ? 1 : 0;
        if constexpr (type == VISIONGRIDTYPE::ENEMYAREA)
            return ownedIndex(loc) != 0 ? 1 : 0; // must be enemy area then
        auto status = occupationStatus(loc);
        if (not status)
            return 0;
        auto [part, playerId] = *status;
        if constexpr (type == VISIONGRIDTYPE::TAIL)
            return part == Player::Part::TAIL && playerId == forPlayer ? 1 : 0;
        if constexpr (type == VISIONGRIDTYPE::ENEMYHEAD)
            return part == Player::Part::HEAD && playerId != forPlayer ? 1 : 0;
        if constexpr (type == VISIONGRIDTYPE::ENEMYTAIL)
            return part == Player::Part::TAIL && playerId != forPlayer ? 1 : 0;

    }
};

#endif // GRID_H
