#ifndef ACTION_H
#define ACTION_H

#include <vector>

enum Action {
    LEFT,
    STRAIGHT,
    RIGHT
};

static std::vector<Action> ALL_ACTIONS({Action::LEFT, Action::STRAIGHT, Action::RIGHT});

#endif // ACTION_H
