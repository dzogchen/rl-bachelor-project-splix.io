#ifndef HELENIXGAME_H
#define HELENIXGAME_H

#include <utility>
#include <optional>
#include <random>
#include <list>
#include <memory>
#include <set>
#include <iostream>
#include <unordered_map>
#include <optional>

#ifdef WEBSOCKETDEBUG
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariantList>
#include <QList>
#endif

#include "grid.h"
#include "player.h"

size_t constexpr MAX_PLAYERS = 70;

template <long Height, long Width>
class HelenixGame {
    Grid<Height, Width> d_grid;
    std::unordered_map<PlayerID, std::shared_ptr<Player>> playerIdToPlayer;
    std::vector<std::pair<Location, Direction>> d_spawnLocations;
    size_t const START_AREA_RADIUS;

    bool d_ended = false;
    static int constexpr KILL_AWARD = 50;
    static int constexpr DEATH_PENALTY = 50;
    static double constexpr STEP_COST = 0.1;
    size_t d_time = 0;

public:
    HelenixGame(std::vector<std::pair<Location, Direction>> const &spawnLocations, size_t startAreaRadius = 2)
      :   d_spawnLocations(spawnLocations), START_AREA_RADIUS(startAreaRadius) {
        spawnPlayersDefault();
    }

    std::shared_ptr<Player> spawnPlayer(Location location, Direction dir);
    void tick();
    auto noPlayers() {
        return playerIdToPlayer.size();
    }
    Grid<Height, Width> const &grid() const {
        return d_grid;
    }
    std::unordered_map<PlayerID, std::shared_ptr<Player>> &players() {
        return playerIdToPlayer;
    }
//    inline void setDirection(int playerID, Direction direction) {
//        playerIdToPlayer[playerID]->direction(direction);
//    }
    inline size_t time() {
        return d_time;
    }
    inline bool ended() {
        return d_ended;
    }

    void reset() {
        playerIdToPlayer.clear();
        d_grid = Grid<Height, Width>{};
        spawnPlayersDefault();
        d_time = 0;
        d_ended = false;
    }
#ifdef WEBSOCKETDEBUG
    QJsonDocument json() {
        QJsonArray ownerGrid;
        QJsonArray occupationGrid;
        for (int row = 0; row != grid().rows(); ++row) {
            QJsonArray rowOwners;
            QJsonArray rowOccupation;
            for (int col = 0; col != grid().cols(); ++col) {
                rowOwners.append(static_cast<int>(grid().ownedIndex(row, col)));
                rowOccupation.append(static_cast<int>(grid().occupiedIndex(row, col)));
            }
            ownerGrid.append(rowOwners);
            occupationGrid.append(rowOccupation);
        }
        QJsonObject jsonObj;
        jsonObj["ownerGrid"] = ownerGrid;
        jsonObj["occupationGrid"] = occupationGrid;

        QJsonArray players;
        for (auto [id, player] : playerIdToPlayer) {
            QJsonObject playerObj;
            playerObj["id"] = static_cast<int>(id);
            playerObj["direction"] = static_cast<int>(player->direction());
            playerObj["points"] = player->points();
            players.append(playerObj);
        }

        jsonObj["players"] = players;
        QJsonDocument jsonDoc(jsonObj);
        return jsonDoc;
    }
#endif

private:
    Location findSpawnLocation();

    std::optional<Player> occupiedBy(Location location);
    std::optional<std::shared_ptr<Player>> ownedBy(Location location);
    bool ownedBy(Location location, std::shared_ptr<Player> player);
    PlayerID newPlayerId() {
        PlayerID id;
        for (id = 1; id < playerIdToPlayer.size() + 1; ++id) {
            if (playerIdToPlayer.count(id) == 0)
                break;
        }
        return id;
    }
    void spawnPlayersDefault() {
        for (auto const& [loc, dir]: d_spawnLocations) {
            spawnPlayer(loc, dir);
        }
    }
};

template <long H, long W>
std::shared_ptr<Player> HelenixGame<H, W>::spawnPlayer(Location location, Direction dir) {
    auto newPlayer = std::make_shared<Player>(location, newPlayerId(), dir);
    d_grid.head(location, newPlayer->id());
    playerIdToPlayer[newPlayer->id()] = newPlayer;

    for (int row = location.row() - START_AREA_RADIUS; row <= location.row() + START_AREA_RADIUS; ++row) {
        for (int col = location.col() - START_AREA_RADIUS; col <= location.col() + START_AREA_RADIUS; ++col) {
            d_grid.owner({row, col}, newPlayer->id());
        }
    }
    newPlayer->givePoints((START_AREA_RADIUS* 2 + 1) * (START_AREA_RADIUS* 2 + 1));
    return newPlayer;
}

template <long H, long W>
void HelenixGame<H, W>::tick() {
    std::set<PlayerID> deadPlayers;

    ++d_time;

    // update player locations, tails and whether squares are occupied
    auto killPlayer = [&deadPlayers](std::shared_ptr<Player> player) {
        player->givePoints(-DEATH_PENALTY); // TODO: generalize
        player->kill();
        deadPlayers.insert(player->id());
    };

    for (auto &[id, player] : playerIdToPlayer) {
        player->move(); // update player location on player
        // death by out of bounds
        if (d_grid.isBorder(player->location())) {
            killPlayer(player);
            continue;
        }

        // update tails        
        // current and previous location is not owned by player
        if (not ownedBy(player->location(), player) and not ownedBy(player->prevLocation(), player)) {
            player->addToTail(player->prevLocation());
            d_grid.tail(player->prevLocation(), player->id());
        } // player entered own area again
        else if (ownedBy(player->location(), player) and not ownedBy(player->prevLocation(), player)) {
            d_grid.tail(player->prevLocation(), player->id());
            player->addToTail(player->prevLocation());

            d_grid.endTail(player, playerIdToPlayer);
            d_grid.resetOccupation(player->prevLocation()); // remove head
            player->resetTail();
        } else {
            d_grid.resetOccupation(player->prevLocation()); // remove head
        }

        // cell is currently occupied
        if (auto occupationStatus = d_grid.occupationStatus(player->location())) {
            auto [part, occupyingPlayerId] = *occupationStatus;
            if (part == Player::Part::TAIL) { // kill occupying player
                killPlayer(playerIdToPlayer[occupyingPlayerId]);
                if (occupyingPlayerId == player->id())
                    continue; // player hit own tail
                player->givePoints(KILL_AWARD);
            }
            if (part == Player::Part::HEAD) {
                if (not ownedBy(player->location(), player))
                    killPlayer(player);
                else
                    player->givePoints(KILL_AWARD);
                killPlayer(playerIdToPlayer[occupyingPlayerId]);
            }
        }
        if (player->isAlive()) {
            d_grid.head(player->location(), player->id());
            player->addReward(-STEP_COST);
        }
    }

    // count number of alive players, if less than 2, end game
    size_t alivePlayers = std::count_if(
        players().begin(),
        players().end(),
        [](auto pair) { auto player = pair.second; return player->isAlive(); });
    if (alivePlayers < 2) {
        d_ended = true;
        return;
    }

    // remove dead players from the grid
    for (int row = 0; row != d_grid.rows(); ++row) {
        for (int col = 0; col != d_grid.cols(); ++col) {
            auto occupationStatus = d_grid.occupationStatus({row, col});
            if (occupationStatus && deadPlayers.count(std::get<1>(*occupationStatus)) != 0)
                d_grid.resetOccupation({row, col});
            if (d_grid.ownedIndex(row, col) != 0 && deadPlayers.count(d_grid.ownedIndex(row, col)) != 0)
                d_grid.resetOwner({row, col});
        }
    }
}

template <long H, long W>
std::optional<std::shared_ptr<Player>> HelenixGame<H, W>::ownedBy(Location location) {
    auto playerId = d_grid.ownedIndex(location);
    if (playerId == 0)
        return {};
    return {playerIdToPlayer[playerId]};
}

template <long H, long W>
bool HelenixGame<H, W>::ownedBy(Location location, std::shared_ptr<Player> player) {
    auto owner = ownedBy(location);
    return owner && owner == player;
}

#endif // HELENIXGAME_H

