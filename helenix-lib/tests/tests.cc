#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <torch/torch.h>

#include <helenixgame.h>
#include <direction.h>

#include <memory>

//TEST_CASE("Create a game") {
//    HelenixGame<200, 200> game{};
//    auto player1 = game.spawnPlayer({100, 100});
//    auto player2 = game.spawnPlayer({100, 99});
//    REQUIRE(game.noPlayers() == 2);

//    SECTION("no collision, locations updated") {
//        player1->direction(Direction::N);
//        player2->direction(Direction::S);
//        game.tick();
//        REQUIRE(game.noPlayers() == 2);
//        REQUIRE(player1->location() == Location{100, 101});
//        REQUIRE(player2->location() == Location{100, 98});
//        player1->direction(Direction::W);
//        player2->direction(Direction::E);
//        game.tick();
//        REQUIRE(game.noPlayers() == 2);
//        REQUIRE(player1->location() == Location{99, 101});
//        REQUIRE(player2->location() == Location{101, 98});
//    }
//}

//TEST_CASE("Hitting border results in death") {
//    HelenixGame<200, 200> game{};
//    auto player1 = game.spawnPlayer({200, 200});
//    game.tick();
//    REQUIRE(not player1->isAlive());
//}

TEST_CASE("One player dies") {
    HelenixGame<3, 3> game{std::vector{std::pair<Location, Direction>(Location{1, 1}, Direction::N)}, 0};
     REQUIRE(game.noPlayers() == 1);
     for (auto [idx, player] : game.players()) {
         REQUIRE(player->isAlive());
     }
     game.tick();
     for (auto [idx, player] : game.players()) {
         REQUIRE(not player->isAlive());
     }
}

TEST_CASE("Two heads colide") {
    HelenixGame<6, 6> game{std::vector{
                               std::pair<Location, Direction>(Location{3, 2}, Direction::E),
                               std::pair<Location, Direction>(Location{3, 4}, Direction::W)
                           }, 0};
    REQUIRE(game.noPlayers() == 2);
    for (auto [idx, player] : game.players()) {
        REQUIRE(player->isAlive());
    }
    game.tick();
    for (auto [idx, player] : game.players()) {
        REQUIRE(not player->isAlive());
        std::cout << player->points() << std::endl;
    }
}

TEST_CASE("Capture three squares") {
    HelenixGame<6, 6> game{std::vector{
                               std::pair<Location, Direction>(Location{3, 2}, Direction::E),
//                               std::pair<Location, Direction>(Location{3, 4}, Direction::W)
                           }, 0};
    game.tick();
    game.players()[1]->go(Action::RIGHT);
    game.tick();
    game.players()[1]->go(Action::RIGHT);
    game.tick();
    game.players()[1]->go(Action::RIGHT);
    game.tick();
    REQUIRE(game.players()[1]->points() == 4);
}

TEST_CASE("Heads-on-heads collision, owned square") {
    HelenixGame<6, 6> game{std::vector{
                               std::pair<Location, Direction>(Location{3, 2}, Direction::E),
                               std::pair<Location, Direction>(Location{5, 4}, Direction::E)
                           }, 0};
    game.tick();
    game.players()[1]->go(Action::RIGHT);
    game.players()[2]->go(Action::LEFT);
    game.tick();
    game.players()[1]->go(Action::RIGHT);
    game.players()[2]->go(Action::STRAIGHT);
    game.tick();
    game.players()[1]->go(Action::RIGHT);
    game.players()[2]->go(Action::LEFT);
    game.tick();
    game.players()[1]->go(Action::RIGHT);
    game.players()[2]->go(Action::STRAIGHT);
    REQUIRE(game.players()[1]->isAlive());
    REQUIRE(not game.players()[2]->isAlive());
}

TEST_CASE("Heads-on-heads collision, owned square, players swapped") {
    HelenixGame<6, 6> game{std::vector{
                               std::pair<Location, Direction>(Location{5, 4}, Direction::E),
                               std::pair<Location, Direction>(Location{3, 2}, Direction::E)
                           }, 0};
    game.tick();
    game.players()[2]->go(Action::RIGHT);
    game.players()[1]->go(Action::LEFT);
    game.tick();
    game.players()[2]->go(Action::RIGHT);
    game.players()[1]->go(Action::STRAIGHT);
    game.tick();
    game.players()[2]->go(Action::RIGHT);
    game.players()[1]->go(Action::LEFT);
    game.tick();
    game.players()[2]->go(Action::RIGHT);
    game.players()[1]->go(Action::STRAIGHT);
    REQUIRE(game.players()[2]->isAlive());
    REQUIRE(not game.players()[1]->isAlive());
}

TEST_CASE("Two players") {
    auto game = std::make_shared<HelenixGame<21, 21>>(std::vector{
        std::pair<Location, Direction>(Location{10, 3}, Direction::E),
        std::pair<Location, Direction>(Location{10, 17}, Direction::W)});
    REQUIRE(game->noPlayers() == 2);
}
