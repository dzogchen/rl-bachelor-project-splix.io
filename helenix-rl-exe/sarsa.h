#ifndef SARSA_H
#define SARSA_H

#include "rlalg.h"
#include "helenixrltypes.h"

#include <torchmlp.h>

#include <memory>
#include <string>

class SARSA: public RLAlgBase
{
    vecType d_state;

    Action d_prevAction;
    Action d_nextAction;

    vecType d_prevEstimate;

public:
    SARSA(std::shared_ptr<RLParams> rlparams);
    void start(vecType const&state) override;
    Action beforeTick() override;
    void afterTick(vecType const&newState, float reward) override;
    void end(float reward) override;

    static std::shared_ptr<TorchMLP> mlp;
    static bool used;
    std::string name() const override;
};

#endif // SARSA_H
