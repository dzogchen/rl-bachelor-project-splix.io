#include "util.h"
#include "helenixrltypes.h"

#include <helenixgame.h>

#include <Eigen/Dense>
//#include <torch/types.h>

#include <random>
#include <iostream>

static std::random_device rd;
static std::mt19937 gen(rd());

Eigen::VectorXd actionToVec(Direction dir) {
    Eigen::VectorXd vec(2);
    switch (dir) {
    case Direction::N:
        vec << 0, 0;
        break;
    case Direction::E:
        vec << 1, 0;
        break;
    case Direction::S:
        vec << 0, 1;
        break;
    case Direction::W:
        vec << 1, 1;
    }
    return vec;
}

Action randomAction() {
    std::uniform_int_distribution<size_t> dis(0, ALL_ACTIONS.size() - 1);
    Action randomAction = ALL_ACTIONS[dis(gen)];
    return randomAction;
}

Action maxAction(Eigen::VectorXd const &qValues) {
    Action maxAction = Action::STRAIGHT;
    double maxQValue = qValues[1];
    for (auto action : ALL_ACTIONS) {
        if (qValues[static_cast<Eigen::Index>(action)] > maxQValue) {
            maxQValue = qValues[static_cast<Eigen::Index>(action)];
            maxAction = action;
        }
    }
    return maxAction;
}

// TODO merge with previous class
Action maxAction(torch::Tensor const &qValues) {
    Action maxAction = Action::STRAIGHT;
    double maxQValue = qValues[1].item<float>();
    for (auto action : ALL_ACTIONS) {
        if (qValues[(size_t) action].item<float>() > maxQValue) {
            maxQValue = qValues[(size_t) action].item<float>();
            maxAction = action;
        }
    }
    return maxAction;
}

Action eGreedyAction(double epsilon, vecType const &qValues) {
    std::bernoulli_distribution smallProb(epsilon); // true with p=0.01

    if (smallProb(gen)) { // return random action
        return randomAction();
    }

    // return greedy action
    return maxAction(qValues);
}

std::vector<double> boltzmannProbs(vecType const &qValues, double temperature)
{
    std::vector<double> bVec;
    double total = 0;
    for (long idx = 0; idx != qValues.size(0); ++idx) {
        total += boltzmann<double>(qValues[idx].item<float>(), temperature);
    }
    for (long idx = 0; idx != qValues.size(0); ++idx) {
        bVec.push_back(boltzmann<double>(qValues[idx].item<float>(), temperature) / total);
    }
    return bVec;
}

Action boltzmannAction(double temperature, vecType const &qValues) {
    std::vector<double> bVec = boltzmannProbs(qValues, temperature);
    std::discrete_distribution<> dist(bVec.begin(), bVec.end());
    return static_cast<Action>(dist(gen));
}

Action maxBoltzmannAction(double temperature, vecType const &qValues) {
    double epsilon = 0.1;
    std::bernoulli_distribution smallProb(epsilon);
    if (smallProb(gen)) {
        return boltzmannAction(temperature, qValues);
    }

    return maxAction(qValues);
}

