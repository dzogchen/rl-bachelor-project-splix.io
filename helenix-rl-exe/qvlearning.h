#ifndef QVLEARNING_H
#define QVLEARNING_H

#include "rlalg.h"
#include "util.h"

#include <torchmlp.h>

#include <torch/torch.h>

#include <memory>

class QVLearning : public RLAlgBase
{
    vecType d_state;

    Action d_action;

    vecType d_qEstimate;

  public:
    QVLearning(std::shared_ptr<RLParams> rlparams);
    void start(vecType const&state) override;
    Action beforeTick() override;
    void afterTick(vecType const&newState, float reward) override;
    void end(float reward) override;
    std::string name() const override;

    static std::shared_ptr<TorchMLP> vMLP;
    static std::shared_ptr<TorchMLP> qMLP;
    static bool used;
};

#endif // QVLEARNING_H
