#ifndef HELENIXRL_H
#define HELENIXRL_H

#include "rlparams.h"
#include "rlalg.h"

#include <helenixgame.h>
#include <torchmlp.h>

#include <tclap/CmdLine.h>
#include <boost/circular_buffer.hpp>
#ifdef WEBSOCKETDEBUG
#include "server_ws.hpp"
using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
#endif

#include <fstream>
#include <memory>
#include <filesystem>
#include <stddef.h> // size_t

size_t const GRIDSIZE = 21;

class HelenixRL
{
    TCLAP::CmdLine d_cmd{"Runs RL on the game of splix.io", ' ', "0.1"};
    TCLAP::ValueArg<std::string> d_sarsaMLPArg{"","sarsaMLP","MLP SARSA",false,"","filename"};
    TCLAP::ValueArg<std::string> d_qlearningMLPArg{"","qlearningMLP","MLP Q-Learning",false,"","filename"};
    TCLAP::ValueArg<std::string> d_dqlearningMLP1Arg{"","dqlearningMLP1","MLP1 Double Q-learning",false,"","filename"};
    TCLAP::ValueArg<std::string> d_dqlearningrandomMLP1Arg{"","dqlearningrandomMLP1","MLP1 Double Q-learning (random)",false,"","filename"};
    TCLAP::ValueArg<std::string> d_dqlearningrandomMLP2Arg{"","dqlearningrandomMLP2","MLP2 Double Q-learning (random)",false,"","filename"};
    TCLAP::ValueArg<std::string> d_dqlearningMLP2Arg{"","dqlearningMLP2","MLP2 Double Q-learning",false,"","filename"};
    TCLAP::ValueArg<std::string> d_qvlearningvMLP{"","vQVMLP","MLP for estimating V in QV-learning",false,"","filename"};
    TCLAP::ValueArg<std::string> d_qvlearningqMLP{"","qQVMLP","MLP for estimating Q in QV-learning",false,"","filename"};
//    TCLAP::ValueArg<double> d_temperatureArg{"","temp","Boltzmann temperature",false,100,"double"};
//    TCLAP::ValueArg<double> d_finalTemperatureArg{"","ftemp","Final boltzmann temperature",false,0.1,"double"};

    TCLAP::SwitchArg d_overwriteArg{"", "overwrite", "Overwrite output dir"};
    TCLAP::SwitchArg d_testArg{"", "test", "Only test algorithms"};
    TCLAP::ValueArg<size_t> d_continueArg{"","continue","Continue with previous amount of episodes",false,0,"integer"};
    TCLAP::MultiArg<size_t> d_hiddenLayerNodesArg{"","hn","Number of hidden layer nodes",false,"integer"};

    std::vector<std::string> allowedAlgs{"sarsa", "q-learning", "double-q-learning","double-q-learning-half", "qv-learning", "dqn"};
    TCLAP::ValuesConstraint<std::string> allowedAlgsVals{allowedAlgs};
    TCLAP::MultiArg<std::string> d_algsArg{"","alg","RL algorithm (specify once per player)",true, &allowedAlgsVals};

    std::vector<std::string> allowedActionSel{"egreedy", "boltzmann", "max-boltzmann"};
    TCLAP::ValuesConstraint<std::string> allowedActionSelVals{allowedActionSel};
    TCLAP::ValueArg<std::string> d_actionSelArg{"","actionsel","Action selection method",false, "egreedy", &allowedActionSelVals};

    std::filesystem::path d_outputdir;
    TCLAP::ValueArg<size_t> d_noOutputDir{"","outdirnum","Prefix for output dir",false,0,"integer"};
    std::ofstream d_avgPointsPerEpisode;
    std::ofstream d_avgMaxPointsPerEpisode;
    std::ofstream d_avgTimePerEpisode;
    std::map<size_t, std::ofstream> d_avgPlayer;


    size_t d_episodes;
    double d_epsilon;
    double d_finalEpsilon;
    double d_epsilonStep;
    double d_learningRate;
    size_t d_vGridSize;
    size_t d_timePerEpisode = 1000;
    double d_discountingFactor;
    double d_finalDiscountingFactor;

#ifdef TEST
    size_t const RUNNING_AVERAGE_EPISODES = 1;
#else
    size_t const RUNNING_AVERAGE_EPISODES = 1000;
#endif
    boost::circular_buffer<size_t> episodeTimes{RUNNING_AVERAGE_EPISODES};
    boost::circular_buffer<int> episodeMaxPoints{RUNNING_AVERAGE_EPISODES};
    boost::circular_buffer<int> episodeAvgPoints{RUNNING_AVERAGE_EPISODES};
    std::map<size_t, boost::circular_buffer<int>> episodeAvgPointsPlayer;

    std::shared_ptr<HelenixGame<GRIDSIZE, GRIDSIZE>> d_game;

    std::map<PlayerID, std::string> d_playerAlgs; // stores string repr. of algorithm per player
    std::map<size_t, std::shared_ptr<RLAlgBase>> d_playerToAlg;
    std::shared_ptr<RLParams> d_rlparams;

#ifdef WEBSOCKETDEBUG
    WsServer d_server;
    std::thread d_server_thread;
    std::shared_ptr<WsServer::Connection> d_currConnection;
#endif

public:
    HelenixRL(int argc, char** argv);
    ~HelenixRL();
    void run();

private:
    void parseArgs(int argc, char** argv);
    void setupNeuralNetworks();
    void setupPlayerAlgs();
    void logEpisode(size_t ep);
    void saveMLPs();
#ifdef WEBSOCKETDEBUG
    void setupWebsocketServer();
    void sendGameState();
#endif

};

#endif // HELENIXRL_H
