#include "rlalg.h"

RLAlgBase::RLAlgBase(std::shared_ptr<RLParams> rlparams)
    : d_rlparams(rlparams),
      d_algNo(algs)
{
    ++algs;
}

RLAlgBase::~RLAlgBase()
{

}

int RLAlgBase::algs = 0;
