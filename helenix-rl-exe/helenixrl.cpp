#include "helenixrl.h"

#include <sarsa.h>
#include "qvlearning.h"
#include "qlearning.h"
#include "dqn.h"
#include <rlparams.h>
#include <dqlearninghalf.h>
#include <dqlearning.h>
#include <util.h>

#include <location.h>

#include <iostream>
#include <utility> // std::pair

HelenixRL::HelenixRL(int argc, char** argv)
{
    parseArgs(argc, argv);
    d_game = std::make_shared<HelenixGame<GRIDSIZE, GRIDSIZE>>(std::vector{
                std::pair<Location, Direction>(Location{10, 3}, Direction::E),
                std::pair<Location, Direction>(Location{10, 17}, Direction::W)});
    assert(d_game->noPlayers() == 2);

    d_outputdir = std::filesystem::path{
            d_algsArg.getValue()[0] + (d_algsArg.getValue().size() > 1 ? "_" + d_algsArg.getValue()[1] : "") + "-" +
            d_actionSelArg.getValue() + "-"
            "e_" + std::to_string(d_episodes) + "-" +
            "epsilon_" + std::to_string(d_epsilon) + "-" +
            "finalEpsilon_" + std::to_string(d_finalEpsilon) + "-" +
            "vgridsize_" + std::to_string(d_vGridSize) + "-" +
            "hn_" + std::to_string(d_hiddenLayerNodesArg.getValue()[0]) + "_" + std::to_string(d_hiddenLayerNodesArg.getValue()[d_hiddenLayerNodesArg.getValue().size() - 1]) + + "-"
            "a_" + std::to_string(d_learningRate) + "-" +
            "discountingFactor_" + std::to_string(d_discountingFactor) + "-" +
            "finalDiscountingFactor_" + std::to_string(d_finalDiscountingFactor)
            };
    if (d_noOutputDir.getValue() != 0) {
        d_outputdir = std::to_string(d_noOutputDir.getValue()) + "_" + d_outputdir.string();
    }
    if (std::filesystem::exists(d_outputdir)) {
        if (d_overwriteArg.getValue()) {
            std::filesystem::remove_all(d_outputdir);
        } else {
            throw std::runtime_error("Output directory exists");
        }
    }

    std::filesystem::create_directory(d_outputdir);

    setupPlayerAlgs();
    setupNeuralNetworks();

    d_avgPointsPerEpisode = std::ofstream(d_outputdir / "running_avg_points_per_episode");
    d_avgMaxPointsPerEpisode = std::ofstream(d_outputdir / "running_avg_max_points_per_episode");
    d_avgTimePerEpisode = std::ofstream(d_outputdir / "running_avg_time_per_episode");
    for (auto [id, player] : d_game->players()) {
        d_avgPlayer[id] = std::ofstream(d_outputdir / ("running_avg_player_" + std::to_string(id) + "_" + d_playerToAlg[id]->name()));
        episodeAvgPointsPlayer.emplace(id, RUNNING_AVERAGE_EPISODES);
    }

#ifdef WEBSOCKETDEBUG
    setupWebsocketServer();
#endif
}

HelenixRL::~HelenixRL() {
#ifdef WEBSOCKETDEBUG
    d_server.stop();
    d_server_thread.join();
#endif
}

void HelenixRL::run() {
    std::map<size_t, double> oldReward; // stores the reward of all the players of the previous iteration,
    std::map<size_t, double> reward;    //  so we can calculate rewards


    for (size_t ep = 1; ep <= d_episodes; ++ep) {
        d_game->reset();

        for (auto [id, player] : d_game->players()) {
            d_playerToAlg[id]->start(getState(*d_game, player, d_vGridSize));
            oldReward[id] = player->reward();
        }
        while (true) {
            for (auto [id, player] : d_game->players()) {
                player->go(d_playerToAlg[id]->beforeTick());
            }


#ifdef WEBSOCKETDEBUG
            sendGameState();
#endif
            d_game->tick();
            for (auto [id, player] : d_game->players()) {
                reward[id] = player->reward() - oldReward[id];
                oldReward[id] = player->reward();
            }
            if (d_game->time() > d_timePerEpisode || d_game->ended()) {
                logEpisode(ep);

                for (auto [id, player] : d_game->players()) {
                    d_playerToAlg[id]->end(reward[id]);
                }
                if (ep % 100000 == 0) { // save MLPs every 100000 eps
                    saveMLPs();
                }
                break;
            }
            for (auto [id, player] : d_game->players()) {
                d_playerToAlg[id]->afterTick(
                            getState(*d_game, player, d_vGridSize),
                            reward[id]);
            }
        }
        d_epsilon += d_epsilonStep;
        d_rlparams->step();
    }
    saveMLPs();
}

void HelenixRL::parseArgs(int argc, char** argv) {

    TCLAP::ValueArg<size_t> episodesArg("e","episodes","Number of episodes",false,10,"integer");
    TCLAP::ValueArg<double> epsilonArg("","epsilon","Epsilon",false,0.4,"double");
    TCLAP::ValueArg<double> finalEpsilonArg("","fepsilon","Final epsilon",false,0.01,"double");
    TCLAP::ValueArg<double> learningRateArg("a","learningrate","Epsilon",false,0.001,"double");
    TCLAP::ValueArg<double> discountingFactorArg("","df","Discounting factor",false,0.99,"double");
    TCLAP::ValueArg<double> finalDiscountingFactorArg("","fdf","Final discounting factor",false,0.99,"double");
    TCLAP::ValueArg<size_t> visionGridSizeArg("","vgridsize","Size of vision grid",false,3,"integer");

    TCLAP::SwitchArg writeMLPArg("w", "write", "Write MLP to file");

    d_cmd.add(episodesArg);
    d_cmd.add(epsilonArg);
    d_cmd.add(finalEpsilonArg);
    d_cmd.add(learningRateArg);
    d_cmd.add(discountingFactorArg);
    d_cmd.add(finalDiscountingFactorArg);
    d_cmd.add(visionGridSizeArg);
    d_cmd.add(d_sarsaMLPArg);
    d_cmd.add(d_qlearningMLPArg);
    d_cmd.add(d_dqlearningMLP1Arg);
    d_cmd.add(d_dqlearningMLP2Arg);
    d_cmd.add(d_dqlearningrandomMLP1Arg);
    d_cmd.add(d_dqlearningrandomMLP2Arg);
    d_cmd.add(d_qvlearningvMLP);
    d_cmd.add(d_qvlearningqMLP);
    d_cmd.add(d_continueArg);
    d_cmd.add(d_algsArg);
    d_cmd.add(d_actionSelArg);
    d_cmd.add(writeMLPArg);
    d_cmd.add(d_overwriteArg);
    d_cmd.add(d_noOutputDir);
    d_cmd.add(d_hiddenLayerNodesArg);

    d_cmd.parse(argc, argv);

    if (d_algsArg.getValue().size() > 2) {
        throw std::runtime_error("More than two players currently not supported.");
    }

    if (visionGridSizeArg.getValue() % 2 == 0) {
        throw std::runtime_error("Provide an even number as vision grid size.");
    }

    if (d_hiddenLayerNodesArg.getValue().size() < 1) {
        throw std::runtime_error("Specify amount of hidden nodes using --hn.");
    }

    d_episodes = episodesArg.getValue();
    d_epsilon = epsilonArg.getValue();
    d_finalEpsilon = finalEpsilonArg.getValue();
    d_epsilonStep = (d_finalEpsilon - d_epsilon) / d_episodes;
    d_learningRate = learningRateArg.getValue();
    d_vGridSize = visionGridSizeArg.getValue();
    d_discountingFactor = discountingFactorArg.getValue();
    d_finalDiscountingFactor = finalDiscountingFactorArg.getValue();
}

void HelenixRL::logEpisode(size_t ep) {
    ep += d_continueArg.getValue();
    int maxPoints = std::numeric_limits<int>::min();
    int avgPoints = 0;
    for (auto [id, player] : d_game->players()) {
        maxPoints = player->points() > maxPoints ? player->points() : maxPoints;
        avgPoints += player->points() / d_game->players().size();
        d_avgPlayer[id] << ep << " " << player->points() << std::endl;
    }
    d_avgTimePerEpisode << ep << " " << d_game->time() << std::endl;
    d_avgMaxPointsPerEpisode << ep << " " << maxPoints << std::endl;
    d_avgPointsPerEpisode << ep << " " << avgPoints << std::endl;
}

#ifdef WEBSOCKETDEBUG
void HelenixRL::setupWebsocketServer() {
    d_server.config.port = 8080;

    auto &endpoint = d_server.endpoint["^/$"];
    endpoint.on_open = [&currConnection = d_currConnection, game = d_game](std::shared_ptr<WsServer::Connection> connection) {
        std::cout << "Server: Opened connection " << connection.get() << std::endl;
        currConnection = connection;
        QJsonDocument gameJsonDoc = game->json();
        for (auto [id, player] : game->players()) {

        }
        currConnection->send(gameJsonDoc.toJson().toStdString());
    };

    d_server_thread = std::thread([&server = d_server]() {
        server.start();
    });

    while (d_server.get_connections().size() == 0) {
        std::cout << "Waiting for connection..." << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}


void HelenixRL::sendGameState()  {
    d_currConnection->send(d_game->json().toJson().toStdString());
    while (true) {
        std::cout << "Press ENTER to continue..." << std::endl;
        int ch = std::cin.get();
        if (ch == '\n')
            break;
//                if (ch == EOF)
//                    return 1;
    }
}
#endif

void HelenixRL::saveMLPs() { // TODO: only write MLP if used
    if (SARSA::used)
        SARSA::mlp->save(d_outputdir / "sarsa.mlp");
    if (QLearning::used) {
        for (size_t idx = 0; idx != QLearning::mlp.size(); ++idx) {
            QLearning::mlp[idx]->save(d_outputdir.string() + "/qlearning" + std::to_string(idx) + ".mlp");
        }
    }
    if (DQLearningHalf::used) {
        DQLearningHalf::mlp1->save(d_outputdir / "dqlearning1.mlp");
        DQLearningHalf::mlp2->save(d_outputdir / "dqlearning2.mlp");
    }
    if (DQLearning::used) {
        DQLearning::mlp1->save(d_outputdir / "dqlearningrandom1.mlp");
        DQLearning::mlp2->save(d_outputdir / "dqlearningrandom2.mlp");
    }
    if (QVLearning::used) {
        QVLearning::vMLP->save(d_outputdir / "qvlearningV.mlp");
        QVLearning::qMLP->save(d_outputdir / "qvlearningQ.mlp");
    }
}

void HelenixRL::setupPlayerAlgs() {
    if (d_algsArg.getValue().size() != 1 && d_algsArg.getValue().size() != d_game->noPlayers())
        throw std::runtime_error("Use --alg to specify one algorithm, or use it multiple times for every player");

    d_rlparams = std::make_shared<RLParams>(d_epsilon, d_discountingFactor, d_finalEpsilon, d_finalDiscountingFactor, d_episodes);
    if (d_actionSelArg.getValue() == "egreedy")
        d_rlparams->actionSel = eGreedyAction;
    else if (d_actionSelArg.getValue() == "boltzmann") {
        d_rlparams->actionSel = boltzmannAction;
    } else if (d_actionSelArg.getValue() == "max-boltzmann") {
        d_rlparams->actionSel = maxBoltzmannAction;
    }

    for (auto [id, player] : d_game->players()) {
        if (d_algsArg.getValue().size() == 1)
            d_playerAlgs[id] = d_algsArg.getValue()[0];
        else
            d_playerAlgs[id] = d_algsArg.getValue()[id - 1];
    }

    for (auto [id, player] : d_game->players()) {
        if (d_playerAlgs[id] == "sarsa") {
            d_playerToAlg[id] = std::make_shared<SARSA>(d_rlparams);
            SARSA::used = true;
        } else if (d_playerAlgs[id] == "q-learning") {
            d_playerToAlg[id] = std::make_shared<QLearning>(d_rlparams);
            QLearning::used = true;
        } else if (d_playerAlgs[id] == "double-q-learning") {
            d_playerToAlg[id] = std::make_shared<DQLearning>(d_rlparams);
            DQLearningHalf::used = true;
        }  else if (d_playerAlgs[id] == "double-q-learning-half") {
            d_playerToAlg[id] = std::make_shared<DQLearningHalf>(d_rlparams);
            DQLearning::used = true;
        } else if (d_playerAlgs[id] == "qv-learning") {
            d_playerToAlg[id] = std::make_shared<QVLearning>(d_rlparams);
            QVLearning::used = true;
        } else if (d_playerAlgs[id] == "dqn") {
            d_playerToAlg[id] = std::make_shared<DQN>(d_rlparams);
            DQN::used = true;
        }
    }
}

void HelenixRL::setupNeuralNetworks() {
    size_t inputSize = // TODO make work for Eigen::VectorXd
        static_cast<size_t>(getState(*d_game, d_game->players().begin()->second, d_vGridSize).size(0));
    size_t hiddenLayerSize = d_hiddenLayerNodesArg.getValue()[0];
    size_t outputSize = ALL_ACTIONS.size(); // one output node for each action

    std::vector<size_t> const nodesPerLayer{inputSize, hiddenLayerSize, outputSize};

    if (SARSA::used) {
        SARSA::mlp = std::make_shared<TorchMLP>(d_learningRate);
        if (d_sarsaMLPArg.getValue() == "") {
            std::cerr << "Creating new SARSA MLP." << std::endl;
            SARSA::mlp->init(nodesPerLayer);
        } else {
            std::cerr << "Loading SARSA MLP from file." << std::endl;
            SARSA::mlp->init(nodesPerLayer, d_sarsaMLPArg.getValue());
        }
    }

    if (QLearning::used) {
        QLearning::mlp[0] = std::make_shared<TorchMLP>(d_learningRate);
        QLearning::mlp[1] = std::make_shared<TorchMLP>(d_learningRate);
        if (d_qlearningMLPArg.getValue() == "") {
            size_t first = d_hiddenLayerNodesArg.getValue()[0];
            size_t second = d_hiddenLayerNodesArg.getValue().size() == 1 ? first : d_hiddenLayerNodesArg.getValue()[1];
            std::cerr << "Creating new Q-Learning MLP. " << first << std::endl;
            QLearning::mlp[0]->init({inputSize, first, outputSize});
            std::cerr << "Creating new Q-Learning MLP. " << second << std::endl;
            QLearning::mlp[1]->init({inputSize, second, outputSize});
        } else {
            std::cerr << "Loading Q-Learning MLP from file. NOPE" << std::endl;
//            QLearning::mlp->init(nodesPerLayer, d_qlearningMLPArg.getValue());
        }
    }

    if (DQLearningHalf::used) {
        DQLearningHalf::mlp1 = std::make_shared<TorchMLP>(d_learningRate);
        DQLearningHalf::mlp2 = std::make_shared<TorchMLP>(d_learningRate);
        if (d_dqlearningMLP1Arg.getValue() == "" || d_dqlearningMLP2Arg.getValue() == "") {
            std::cerr << "Creating new Double Q-Learning (half) MLPs." << std::endl;
            DQLearningHalf::mlp1->init({inputSize, hiddenLayerSize / 2, outputSize});
            DQLearningHalf::mlp2->init({inputSize, hiddenLayerSize / 2, outputSize});
        } else {
            std::cerr << "Loading Double Q-Learning MLPs from file." << std::endl;
            DQLearningHalf::mlp1->init(nodesPerLayer, d_dqlearningMLP1Arg.getValue());
            DQLearningHalf::mlp2->init(nodesPerLayer, d_dqlearningMLP2Arg.getValue());
        }
    }

    if (DQLearning::used) {
        DQLearning::mlp1 = std::make_shared<TorchMLP>(d_learningRate);
        DQLearning::mlp2 = std::make_shared<TorchMLP>(d_learningRate);
        if (d_dqlearningMLP1Arg.getValue() == "" || d_dqlearningrandomMLP2Arg.getValue() == "") {
            std::cerr << "Creating new Double Q-Learning MLPs." << std::endl;
            DQLearning::mlp1->init(nodesPerLayer);
            DQLearning::mlp2->init(nodesPerLayer);
        } else {
            std::cerr << "Loading Double Q-Learning (random) MLPs from file." << std::endl;
            DQLearning::mlp1->init(nodesPerLayer, d_dqlearningrandomMLP1Arg.getValue());
            DQLearning::mlp2->init(nodesPerLayer, d_dqlearningrandomMLP2Arg.getValue());
        }
    }

    if (QVLearning::used) {
        QVLearning::vMLP = std::make_shared<TorchMLP>(d_learningRate);
        QVLearning::qMLP = std::make_shared<TorchMLP>(d_learningRate);
        if (d_qvlearningvMLP.getValue() == "" || d_qvlearningqMLP.getValue() == "") {
            std::cerr << "Creating new QV-learning MLPs." << std::endl;
            QVLearning::vMLP->init({inputSize, hiddenLayerSize, 1});
            QVLearning::qMLP->init(nodesPerLayer);
        } else {
            std::cerr << "Loading QV-learning MLPs from file." << std::endl;
            QVLearning::vMLP->init({inputSize, hiddenLayerSize, 1}, d_qvlearningvMLP.getValue());
            QVLearning::qMLP->init(nodesPerLayer, d_qvlearningqMLP.getValue());
        }
    }

    if (DQN::used) {
        DQN::mlp = std::make_shared<TorchMLP>(d_learningRate);
        std::cerr << "Creating new DQN MLP." << std::endl;
        DQN::mlp->init(nodesPerLayer);
    }
}

