#ifndef DQLEARNINGHALF_H
#define DQLEARNINGHALF_H

#include "rlalg.h"
#include "helenixrltypes.h"

#include <torchmlp.h>

#include <memory>

class DQLearningHalf: public RLAlgBase
{
    vecType d_state;

    Action d_action;

    vecType d_qEstimate1;
    vecType d_qEstimate2;

public:
    DQLearningHalf(std::shared_ptr<RLParams> rlparams);
    void start(vecType const&state) override;
    Action beforeTick() override;
    void afterTick(vecType const&newState, float reward) override;
    void end(float reward) override;
    std::string name() const override;

    static std::shared_ptr<TorchMLP> mlp1;
    static std::shared_ptr<TorchMLP> mlp2;
    static bool used;
    static int count; // amount of DQLearning objects
};

#endif // DQLEARNINGHALF_H
