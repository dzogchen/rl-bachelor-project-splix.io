#ifndef RLALG_H
#define RLALG_H

#include "rlparams.h"
#include "action.h"
#include "helenixrltypes.h"

#include <direction.h>

#include <Eigen/Dense>

#include <memory>
#include <string>

class RLAlgBase
{
protected:
    std::shared_ptr<RLParams> d_rlparams;
    int d_algNo; // number of this algorithm (0-indexed)
    static int algs; // total amount of algs so far

public:
    RLAlgBase(std::shared_ptr<RLParams> rlparams);
    virtual ~RLAlgBase();

    virtual void start(vecType const&state) = 0;
    virtual Action beforeTick() = 0;
    virtual void afterTick(vecType const&newState, float reward) = 0;
    virtual void end(float reward) = 0;
    virtual std::string name() const = 0;
};

#endif // RLALG_H
