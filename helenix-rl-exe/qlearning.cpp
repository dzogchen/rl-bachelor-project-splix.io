#include "qlearning.h"

#include "util.h"

QLearning::QLearning(std::shared_ptr<RLParams> rlparams)
    : RLAlgBase(rlparams)
{

}

void QLearning::start(vecType const&state) {
    d_state = state;
    d_estimate = mlp[d_algNo]->forward(state);
}

Action QLearning::beforeTick() {
    d_action = d_rlparams->actionSel(d_rlparams->epsilon(), d_estimate);
    return d_action;
}

void QLearning::afterTick(vecType const&newState, float reward) {
    vecType newQValues = mlp[d_algNo]->forward(newState);
    d_estimate[static_cast<long>(d_action)] = reward + d_rlparams->discountingFactor() * newQValues[maxAction(newQValues)];
#ifndef TEST
    mlp[d_algNo]->train({d_state, d_estimate});
#endif

    d_state = newState;
    d_estimate = newQValues;
}

void QLearning::end(float reward) {
    d_estimate[static_cast<long>(d_action)] = reward;
#ifndef TEST
    mlp[d_algNo]->train({d_state, d_estimate});
#endif
}

std::string QLearning::name() const {
    return "Q-Learning";
}

std::array<std::shared_ptr<TorchMLP>, 2> QLearning::mlp;
bool QLearning::used = false;
