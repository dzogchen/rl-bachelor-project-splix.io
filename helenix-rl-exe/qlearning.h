#ifndef QLEARNING_H
#define QLEARNING_H

#include "rlalg.h"
#include "helenixrltypes.h"

#include <torchmlp.h>

#include <memory>
#include <string>
#include <array>

class QLearning: public RLAlgBase
{
    vecType d_state;

    Action d_action;

    vecType d_estimate;

public:
    QLearning(std::shared_ptr<RLParams> rlparams);
    void start(vecType const&state) override;
    Action beforeTick() override;
    void afterTick(vecType const&newState, float reward) override;
    void end(float reward) override;

    static std::array<std::shared_ptr<TorchMLP>, 2> mlp;
    static bool used;
    std::string name() const override;
};

#endif // QLEARNING_H
