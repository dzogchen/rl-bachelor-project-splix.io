// 
$( document ).ready(function() {
  window.colorArray = [
    {},
    {dark: "#ff3300", light: "#ffad99"}, // red
    {dark: "#0000ff", light: "#b3b3ff"} // blue
  ];

  window.borderColor = "#e6e6e6";

  //grid width and height
  window.bw = 1000;
  window.bh = 1000;
  //padding around grid
  window.p = 0;
  //size of canvas
  window.cw = bw + (p*2) + 1;
  window.ch = bh + (p*2) + 1;

  window.canvas = document.getElementById('canvas');

  window.context = canvas.getContext('2d');
});

function drawGame() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.beginPath();
  
  fillGrid(bw, bh, 21, 21);
  // drawText(bw, bh, 21, 21);
  drawGrid(bw, bh, 21, 21);
  
}

function isHead(num) {
  return num > 70;
}

function isBorder(row, col) {
  return row == 0 || row == game.ownerGrid.length - 1 ||
    col == 0 || col == game.ownerGrid[0].length - 1;
}

function occupiedBy(row, col) {
  let occIndex = game.occupationGrid[row][col];
  return isHead(occIndex) ? occIndex - 70 : occIndex;
}

function drawGrid(width, height, rows, cols, linewidth = 0.5) {
    for (var x = 0; x <= height; x += (height / rows)) { // horizontal lines
        context.moveTo(p, linewidth + x + p);
        context.lineTo(bw + p, linewidth + x + p);
    }

    for (var x = 0; x <= width; x += (width / cols)) { // vertical lines
      context.moveTo(linewidth + x + p, p);
      context.lineTo(linewidth + x + p, bh + p);
    }
    context.strokeStyle = "black";
    context.stroke();
}

function fillGrid(width, height, rows, cols, linewidth = 0.5) {
  for (var row = 0; row != game.ownerGrid.length; ++row) {
    for (var col = 0; col != game.ownerGrid[row].length; ++col) {
      let cell = {
        x: col * (width / cols),
        y: row * (height / rows)
      };
      let delta = {
        x: (width / cols),
        y: (height / rows)
      }

      if (game.ownerGrid[row][col] != 0 || isBorder(row, col)) {
        context.fillStyle = isBorder(row, col)
          ? borderColor
          : colorArray[game.ownerGrid[row][col]].light;
        context.fillRect(
          p + 1 + cell.x,
          p + 1 + cell.y,
          (width / cols) - 1,
          (height / rows) - 1);
      }


      if (game.occupationGrid[row][col] != 0) {
        context.fillStyle = colorArray[occupiedBy(row, col)].dark;
        if (isHead(game.occupationGrid[row][col])) { // draw head
          let cellDelta = {
            x: delta.x / 5,
            y: delta.y / 5
          }
          context.beginPath();

          let triangle = [
            {x: [1, 2.5, 4], y: [4, 1, 4]}, // up
            {x: [1, 4, 1], y: [1, 2.5, 4]}, // right
            {x: [4, 2.5, 1], y: [1, 4, 1]}, // down
            {x: [4, 1, 4], y: [4, 2.5, 1]}, // left
          ]
          console.log("occupiedBy", occupiedBy(row, col))
          let playerID = occupiedBy(row, col);
          console.log(game.players, playerID)
          let player = _.find(game.players, p => p.id == playerID);
          let dir = player.direction;
          context.strokeStyle = colorArray[occupiedBy(row, col)].dark;
          console.log(context.strokeStyle)
          context.moveTo(
            cell.x + cellDelta.x * triangle[dir].x[0],
            cell.y + cellDelta.y * triangle[dir].y[0]);
          context.lineTo(
            cell.x + cellDelta.x * triangle[dir].x[1],
            cell.y + cellDelta.y * triangle[dir].y[1]);
          context.lineTo(
            cell.x + cellDelta.x * triangle[dir].x[2],
            cell.y + cellDelta.y * triangle[dir].y[2]);
          context.closePath();
          context.fill();
          // context.stroke();

        } else { // draw tail
          let cellDelta = {
            x: delta.x / 6,
            y: delta.y / 6
          }
          context.fillRect(
            p + 1 + cell.x + cellDelta.x * 2,
            p + 1 + cell.y + cellDelta.y * 2,
            2 * cellDelta.x,
            2 * cellDelta.y);
        }
        context.stroke();
      }
    }
  }
}

function drawText(width, height, rows, cols) {
  for (var row = 0; row != game.ownerGrid.length; ++row) {
    for (var col = 0; col != game.ownerGrid[row].length; ++col) {
      let cell = {
        x: col * (width / cols),
        y: row * (height / rows)
      };
      context.font = '6px monospace';
      context.fillStyle = "black";
      context.fillText("(" + row + ", " + col + ")", cell.x + 2, cell.y+ 10);
    }
  }

}
